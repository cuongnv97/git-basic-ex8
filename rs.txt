- fork repo sau do clone source code về máy
	git clone https://gitlab.com/cuongnv97/git-basic-ex8.git

- điều hướng vào thư mục git-basic-ex8
	cd git-basic-ex8

- xem log commit
	git log

- undo 3 commit gần nhất
	git reset --hard HEAD~3
